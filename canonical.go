package main

import (
	"encoding/hex"
	"flag"
	"fmt"
	"hash/fnv"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gosuri/uilive"
	"github.com/viktomas/godu/files"
)

type printOptions struct {
	nullTerminate bool
	debugOutput   bool
}

func createPrintOptions(nullTerminate bool, debugOutput bool) printOptions {
	if debugOutput && nullTerminate {
		log.Fatalln("debug mode can't be used with null terminated output")
	}
	return printOptions{nullTerminate, debugOutput}
}

func checkErr(err error) {
	if err != nil {
		// panic(err.Error())
		log.Fatalln(err.Error())
	}
}

type addFile func(filesBySize map[int64][]*files.File, file *files.File)

func addCanonical(filesBySize map[int64][]*files.File, file *files.File) {
	sameSizeFiles, exists := filesBySize[file.Size]
	if !exists {
		filesBySize[file.Size] = []*files.File{file}
	}
	filesBySize[file.Size] = append(sameSizeFiles, file)
}

func addOtherFile(filesBySize map[int64][]*files.File, file *files.File) {
	sameSizeFiles, exists := filesBySize[file.Size]
	if exists {
		filesBySize[file.Size] = append(sameSizeFiles, file)
	}
}

func cleanSingleFiles(filesBySize map[int64][]*files.File) [][]*files.File {
	filesWithSameSize := [][]*files.File{}
	for _, files := range filesBySize {
		if len(files) > 1 {
			filesWithSameSize = append(filesWithSameSize, files)
		}
	}
	return filesWithSameSize
}

type hashFunction func(path string) string

func getHashForAFile(filePath string) string {
	hasher := fnv.New64a()
	f, err := os.Open(filePath)
	if err != nil {
		log.Println(err.Error())
		return string(rand.Int63())
	}
	defer f.Close()
	_, err = io.Copy(hasher, f)
	if err != nil {
		log.Println(err.Error())
		return string(rand.Int63())
	}

	return (hex.EncodeToString(hasher.Sum(nil)))
}

func hashTheFiles(filesWithSameSize [][]*files.File, hf hashFunction) map[string][]*files.File {
	filesByHash := make(map[string][]*files.File)
	for _, sameSize := range filesWithSameSize {
		for _, file := range sameSize {
			hash := hf(file.Path())
			sameHashFiles, exists := filesByHash[hash]
			if !exists {
				filesByHash[hash] = []*files.File{file}
			}
			filesByHash[hash] = append(sameHashFiles, file)
		}
	}
	return filesByHash
}

func processDir(filesBySize map[int64][]*files.File, folder *files.File, addFile addFile) {
	for _, file := range folder.Files {
		if file.IsDir {
			processDir(filesBySize, file, addFile)
		} else {
			addFile(filesBySize, file)
		}
	}
}

func parseFolder(folderName string, readDir files.ReadDir, ignoreFunction files.ShouldIgnoreFolder) *files.File {
	progress := make(chan int)
	go reportProgress(progress)
	folder := files.WalkFolder(folderName, readDir, ignoreFunction, progress)
	folder.Name = folderName
	return folder
}

func parseFoldersFromArgs(args []string) ([]string, error) {
	absolutePaths := []string{}
	for _, arg := range args {
		absolutePath, err := filepath.Abs(arg)
		if err != nil {
			return nil, err
		}
		absolutePaths = append(absolutePaths, absolutePath)
	}
	return absolutePaths, nil
}

func findAllDuplicates(absolutePaths []string, readDir files.ReadDir, hashFunction hashFunction) [][]*files.File {
	canonicalFolderName, otherFolders := absolutePaths[0], absolutePaths[1:]
	canonicalFolder := parseFolder(canonicalFolderName, readDir, func(string) bool { return false })
	ignoreCanonical := func(absolutePath string) bool {
		return absolutePath == canonicalFolderName
	}
	rootFolder := parseFolder(otherFolders[0], readDir, ignoreCanonical)
	// already sorted by filename
	filesBySize := make(map[int64][]*files.File)
	processDir(filesBySize, canonicalFolder, addCanonical)
	processDir(filesBySize, rootFolder, addOtherFile)
	filesWithSameSize := cleanSingleFiles(filesBySize)
	filesByHash := hashTheFiles(filesWithSameSize, hashFunction)
	duplicateFiles := [][]*files.File{}
	for _, files := range filesByHash {
		if len(files) > 1 {
			duplicateFiles = append(duplicateFiles, files)
		}
	}
	return duplicateFiles
}

func splitCanonicalAndOthers(canonicalFolderName string, files []*files.File) (canonical []*files.File, others []*files.File) {
	for _, file := range files {
		if strings.HasPrefix(file.Path(), canonicalFolderName) {
			canonical = append(canonical, file)
		} else {
			others = append(others, file)
		}
	}
	return canonical, others
}

func printFiles(files []*files.File, nullTerminate bool) {
	var printFunc func(s string)
	if nullTerminate {
		printFunc = func(s string) {
			fmt.Printf("%s\x00", s)
		}
	} else {
		printFunc = func(s string) {
			fmt.Println(s)
		}
	}
	for _, file := range files {
		printFunc(file.Path())
	}
}

func printDuplicateFiles(canonicalFolderName string, duplicateFiles [][]*files.File, options printOptions) {
	var size int64
	for _, files := range duplicateFiles {
		canonical, others := splitCanonicalAndOthers(canonicalFolderName, files)
		// if it's all others or all canonical
		if len(others) == 0 || len(canonical) == 0 {
			continue
		}
		size += files[0].Size
		if options.debugOutput {
			fmt.Println("-----------")
			fmt.Println("Canonical files:")
			printFiles(canonical, false)
			fmt.Println("Other files:")
		}
		printFiles(others, options.nullTerminate)
	}
	fmt.Fprintf(os.Stderr, "Total size (MB): %v\n", size/1024/1024)
}

func validateArgs(args []string) {
	if len(args) != 2 {
		// TODO support multiple folders
		log.Fatalln("You have to specify two folders")
	}
}

func main() {
	log.SetOutput(os.Stderr)
	// limit := flag.Int64("l", 10, "show only files larger than limit (in MB)")
	nullTerminate := flag.Bool("print0", false, "print null-terminated strings")
	debug := flag.Bool("d", false, "show extended debug output")
	flag.Parse()
	args := flag.Args()
	options := createPrintOptions(*nullTerminate, *debug)
	validateArgs(args)
	absolutePaths, err := parseFoldersFromArgs(args)
	checkErr(err)
	duplicateFiles := findAllDuplicates(absolutePaths, ioutil.ReadDir, getHashForAFile)
	canonicalFolderName := absolutePaths[0]
	printDuplicateFiles(canonicalFolderName, duplicateFiles, options)
}

func reportProgress(progress <-chan int) {
	const interval = 50 * time.Millisecond
	writer := uilive.New()
	writer.Out = os.Stderr
	writer.Start()
	defer writer.Stop()
	total := 0
	ticker := time.NewTicker(interval)
	for {
		select {
		case c, ok := <-progress:
			if !ok {
				return
			}
			total += c
		case <-ticker.C:
			fmt.Fprintf(writer, "Walked through %d folders\n", total)
		}
	}
}

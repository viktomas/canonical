module gitlab.com/viktomas/canonical

go 1.13

require (
	github.com/gosuri/uilive v0.0.4
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/viktomas/godu v1.3.1-0.20200502200809-44aea9cf24f3
)

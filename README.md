# `canonical`

**Warning: this is work in progress, always review the result before piping it to something like `rm -rf`**

CLI tool to find duplicate files. Mark one folder as the source of truth and find all files that are duplicate.

Example scenario would be when you have all photos in your Dropbox at `~/Dropbox/Camera Uploads`, you find where else do you have the exact same picture files by running `canonical ~/Dropbox/Camera Uploads ~/`.

## Install and run

`go get -u gitlab.com/viktomas/canonical`

`canonical where/the/originals/are look/for/duplicates/here`

Output is list of files in `look/for/duplicates/here` that are duplicates of a file in `where/the/originals/are`

## Example

- a
  - b
    - c (100KB)
    - d
      - e (50KB)
      - f (30KB)
      - g 
        - h (100KB) **binary-wise identical to `c`**
        - i (20KB)

```bash
> canonical a/b/d/g a/
a/b/c
```

In other words we said "look inside `a/` for files that would be duplicates of files from `a/b/d/g`.

## Current implementation

Filesystem is crawled using `github.com/viktomas/godu/files` module. Then `canonical` groups all files by size and computes hashes for files that could be identical with some canonical files.

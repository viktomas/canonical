package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"testing"
	"time"

	"github.com/viktomas/godu/files"

	"github.com/stretchr/testify/assert"
)

func TestHappy(t *testing.T) {
	rootFolder := files.NewTestFolder("/a",
		files.NewTestFile("hello", 5),
		files.NewTestFolder("foo"),
	)
	canonicalFolder := files.NewTestFolder("/a/foo",
		files.NewTestFile("hello", 5), files.NewTestFile("bar", 6))
	// already sorted by filename
	filesBySize := make(map[int64][]*files.File)
	processDir(filesBySize, canonicalFolder, addCanonical)
	processDir(filesBySize, rootFolder, addOtherFile)
	filesWithSameSize := cleanSingleFiles(filesBySize)

	assert.Equal(t, canonicalFolder.Files[0], filesWithSameSize[0][0])
}

type fakeFile struct {
	fileName  string
	fileSize  int64
	fakeFiles []fakeFile
}

func (f fakeFile) Name() string       { return f.fileName }
func (f fakeFile) Size() int64        { return f.fileSize }
func (f fakeFile) Mode() os.FileMode  { return 0 }
func (f fakeFile) ModTime() time.Time { return time.Now() }
func (f fakeFile) IsDir() bool        { return len(f.fakeFiles) > 0 }
func (f fakeFile) Sys() interface{}   { return nil }

func createReadDir(ff fakeFile) files.ReadDir {
	return func(path string) ([]os.FileInfo, error) {
		names := strings.Split(path, "/")
		fakeFolder := ff
		var found bool
		for _, name := range names {
			found = false
			for _, testFile := range fakeFolder.fakeFiles {
				if testFile.fileName == name {
					fakeFolder = testFile
					found = true
					break
				}
			}
			if !found {
				return []os.FileInfo{}, fmt.Errorf("file not found")
			}

		}
		result := make([]os.FileInfo, len(fakeFolder.fakeFiles))
		for i, resultFile := range fakeFolder.fakeFiles {
			result[i] = resultFile
		}
		return result, nil
	}
}

func TestE2E(t *testing.T) {
	testStructure := fakeFile{"a", 0, []fakeFile{
		{"b", 0, []fakeFile{
			{"c", 100, []fakeFile{}},
			{"d", 0, []fakeFile{
				{"e", 50, []fakeFile{}},
				{"f", 30, []fakeFile{}},
				{"g", 70, []fakeFile{
					{"c", 100, []fakeFile{}},
					{"i", 20, []fakeFile{}},
				}},
			}},
		}},
	}}
	dummyHashFn := func(path string) string {
		segments := strings.Split(path, "/")
		return segments[len(segments)-1]
	}
	absolutePaths := []string{"b/d", "b"}
	result := findAllDuplicates(absolutePaths, createReadDir(testStructure), dummyHashFn)
	assert.Equal(t, 2, len(result[0]))
}

func TestRealFiles(t *testing.T) {
	absolutePaths := []string{"test/folder/subfolder", "test"}

	duplicateFiles := findAllDuplicates(absolutePaths, ioutil.ReadDir, getHashForAFile)
	var paths []string
	for _, file := range duplicateFiles[0] {
		paths = append(paths, file.Path())
	}
	sort.Strings(paths)

	assert.Equal(t, 3, len(paths))
	assert.Equal(t, []string{
		"test/a.txt", "test/folder/e.txt", "test/folder/subfolder/c.txt",
	}, paths)
}
